Gem::Specification.new do |spec|
  spec.name = "exif"
  spec.version = "0.1.2"
  spec.author = "Ryuichi Tamura"
  spec.email = "r-tam@fsinet.or.jp"
  spec.description = %q{Ruby library for EXIF tag parsing}
  spec.summary = %q{Ruby library for EXIF tag parsing}
  spec.homepage = ""
  spec.license = "LGPL-2+"
  s.extension = "extconf.rb"

end
