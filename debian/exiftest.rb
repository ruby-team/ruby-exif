#!/usr/bin/ruby

require 'exif'

if ARGV.size < 1
  STDERR.puts "Usage: #{$0} filename [filenames...]"
  exit 0
end

ARGV.each do |f|
  exif = Exif.new(f)
  puts "#{f}:"
  exif.each_entry { |a,b| printf "TAG %-40.40s %s\n",a,b }
end
