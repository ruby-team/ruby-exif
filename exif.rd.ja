=begin

= ruby-libexif - libexif を ruby から使うためのライブラリ。

libexifはEXIFフォーマットのイメージからタグ情報を抽出するためのCライブラリです。本ライブラリは(({exif(1)}))と同等の機能をrubyから手軽に扱うことを目的としています。

libexif, exifについては、((<URL:http://libexif.sourceforge.net>))を参照して下さい。

= 使い方

 require 'exif'
 
 # カレントディレクトリのimage.jpgからExifオブジェクトを生成する。
 exif = Exif.new('image.jpg')
 # Exifオブジェクトを生成し、データを与える。
 exif = Exif.new
 File.open('image.jpg') { |f| exif << f.read }

 # タグのタイトルを用いて値を得る。
 p exif["Manufacturer"] # -> "FUJIFILM"
 # タグのIDを用いて値を得る。
 p exif['0xa001'] # -> "FUJIFILM"
 #
 # 現在のところ、返り値は文字列オブジェクトで統一されています。
 # また、タグとして、次の3つの表現を与えることができます。
 # exif["Manufacturer"] ; タグのタイトル
 # exif["Make"]         ; タグのEXIF-2.1で規定された名前
 # exif[0xa001]       ; タグのIDを16進表記した文字列
 # IFDによって規定されているタグの種類は異なりますが、
 # (({Exif#ifd=}))によって明示的にIFDを指定しない限り、全てのIFDを対象にして
 # タグを探し出し、対応する値を返そうとします。明示的にIFDを指定した後、
 # そのような動作に戻したいときは、(({Exif#ifd=}))の引数に 
 # (({Exif::IFD::Any}))(暫定的に用意したモジュール定数)を指定して下さい。
 #
 # タグの詳細については、(({Exif.[]}))を用いて調べることができます。
 # また、あるIFDにおいて利用可能なタグについては(({Exif#list_tags}))を
 # 用いて調べることができます。)

 # サムネイルを抽出し、ファイルに保存する。
 File.open('thumb.jpg', 'wb') {|dest| exif.thumbnail(dest)}
 # サムネイルを設定する。既に存在するサムネイルは破棄される。
 File.open('src.jpg', 'rb'){|src| exif.thumbnail = src.read }

= リファレンス

== class Exif

=== クラスメソッド

--- Exif.new

イメージデータが空のExifオブジェクトを生成します。

--- Exif.new(fpath)

(({fpath}))で指定されるイメージを読み、Exifオブジェクトを生成します。イメージがEXIFフォーマットではない場合、例外((<Exif::Error::NotExifFormat>))が発生します。

--- Exif.[](tagid)

文字列か数値で指定される((|tagid|))に関する説明を得ます。返り値は

 ['タグのタイトル', ['タグの名前', 'タグの説明', 'タグID(数値)']]

です。


=== メソッド

--- Exif#load(fpath)

--- Exif#data=(str)
--- Exif#<<(str)

rubyのStringオブジェクトで与えられたデータを新たにセットします。すでにデータがセットされていた場合には、それは破棄されます。

--- Exif#list_tags(ifd)

IFD ((|ifd|)) で定義されているタグのリストを配列で返します。各要素は

 ["タグのタイトル", "タグID('0xdddd')"]

という配列です。((|ifd|))には((<Exif::IFD>))以下で定義される定数を用いて下さい。

--- Exif#byte_order

イメージのバイトオーダーを返します。返り値は((<Exif::ByteOrder>))で定義される定数の値です。

--- Exif#ifd

現在のIFD(Image File Directory)を文字列で返します。

--- Exif#ifd=(ifd)

IFD(Image File Directory)を設定します。((|ifd|))は((<Exif::IFD>))以下で定義される定数を用いて下さい。

--- Exif#[](tag)

全てのIFDに渡って与えられたタグ(({tag}))を調べ、これに対応する値を文字列で返します。ただし、((<Exif#ifd=>))で明示的にIFDを指定した後は、そのIFDにおいてタグが定義されていれば、対応する値を返します。定義されていない場合には、例外((<Exif::Error::TagNotFound>))が発生します。初期動作に戻したい場合は、((<Exif#ifd=>))に((<Exif::IFD::Any>))を指定して下さい。

--- Exif#each_entry(use_tag_id=false) {|tag, value| ...}

イメージに含まれるタグのタイトルを(({tag}))、値を(({tag}))として、ブロックを実行します。((|use_tag_id|))が(({true}))のときは(({tag}))にはタグのID('0xdddd'表記)が代入されます。

--- Exif#extract_thumbnail(dest)
--- Exif#thumbnail(dest)

イメージに含まれるサムネイルを((|dest|))に(({dest#<<}))を用いて書きこみます。

== モジュール

== Exif::IFD

IFDの種類を表す定数

--- Exif::IFD::Zero
--- Exif::IFD::One
--- Exif::IFD::EXIF
--- Exif::IFD::GPS
--- Exif::IFD::Interoperability
--- Exif::IFD::Any

== Exif::ByteOrder

EXIFイメージのバイトオーダーを表す定数

--- Exif::ByteOrder::INTEL
--- Exif::ByteOrder::MOTOROLA

== 例外クラス

 RuntimeError --- Exif::Error -+- Exif::Error::NotExifFormat
                               |- Exif::Error::InvalidIFD
                               `- Exif::Error::TagNotFound
--- Exif::Error

ruby-libexif起源の例外は全てこの例外か、または以下のサブクラスの例外に属します。

--- Exif::Error::ExifFormat

与えられたイメージがEXIFフォーマットに適合していない場合、この例外が発生します。

--- Exif::Error::InvalidIFD

((<Exif#ifd=>))で、不正なIFDを指定した場合に、この例外が発生します。

--- Exif::Error::TagNotFound

((<Exif#[]>))で、Tagが現在のIFDで見つからなかった場合に、この例外が発生します。

--- Exif::Error::ThumbnailNotFound

((<Exif#thumbnail>))で、イメージにサムネイルが含まれない場合に、この例外が発生します。

=end
