require 'mkmf'

dir_config("exif", "/usr/local/include", "/usr/local/lib")

def have_libexif_header
  have_header('libexif/exif-ifd.h') and
  have_header('libexif/exif-data.h') and
  have_header('libexif/exif-note.h') and
  have_header('libexif/exif-utils.h') and
  have_header('libexif/exif-tag.h') 
end

if have_libexif_header and have_library("exif")
  create_makefile("exif")
end

