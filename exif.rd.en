=begin

= ruby-libexif - an interface to libexif library

((<libexif|URL:http://libexif.sourceforge.net>)) is the library for parsing image information contained in EXIF format images. ruby-libexif provides a simple interface to this library.

= Usage

 require 'exif'
 
 #
 # generate Exif object from 'image.jpg' in the current directory.
 #  
 exif = Exif.new('image.jpg')
 #
 # generate an empty Exif object, and load the image data into it.
 #
 exif = Exif.new
 File.open('image.jpg') { |f| exif << f.read }
 

 #
 # To access the value of the tag, use Exif#[].
 # The return value are string object. 
 # You can specify a tag by its name or title, or ID.
 # For example, if you want to know the manufacturer of
 # the recording equipment that generated the image, 
 # you can get the information by specifying the tag as follows:
 #
 # * exif['Manufacturer']  ; "tag title"
 # * exif['Make']          ; "tag name"
 # * exif[0x010f]        ; "tag ID"
 #
(Please refer ((<Exif.[]>)) , ((<Exif#[]>)) for more details, and ((<Exif#list_tags>)) for available tags in the IFD.) 
 # get a value of the specifid tag by its title
 p exif["Manufacturer"] # -> "FUJIFILM"
 # get a value of the specified tag by its tag ID (string)
 p exif[0xa001] # -> "FUJIFILM"


 #
 # If the image has thumbnail, you can extract it by
 # Exif#extract_thumbnail, or Exif#thumbnail
 #

 # extract a thumbnail, and save it to a file.
 File.open('thumb.jpg', 'wb') {|dest| exif.thumbnail(dest)}
 # set a new thumbnail to Exif object. note that existing 
 # thumbnail, if exists,  will be destroyed. 
 File.open('src.jpg', 'rb'){|src| exif.thumbnail = src.read }

= Reference

== class Exif

=== class methods

--- Exif.new

generate a new Exif object with empty data.

--- Exif.new(fpath)

load the image data from (({fpath})), and generates a new Exif object. If the image is not EXIF-formatted, exception ((<Exif::Error::NotExifFormat>)) raises.

--- Exif.[](tagid)

get a description of ((|tagid|)). the returned value is an array object:

 ['tag title', ['tag name', 'description of the tag', 'tag ID']]

=== instance methods

--- Exif#data=(str)
--- Exif#<<(str)

set the image data given as String object. The existing data in the object, if exists, will be destroyed.

--- Exif#list_tags(ifd)

return an array consists of tags defiend in IFD ((|ifd|)). each elements is following array object:

 ["tag title", "tag ID('0xdddd')"]

((|ifd|)) is one of the constants defined in ((<Exif::IFD>)).

--- Exif#byte_order

--- Exif#ifd

return a string that represents current IFD. 

--- Exif#ifd=(ifd)

set IFD of the object to ((|ifd|)). ((|ifd|)) is one of the constants defined in ((<Exif::IFD>)), or either one of "0", "1", "GPS", "Interoperability". If invalid IFD is given, exception ((<Exif::Error::InvalidIFD>)) raises.

--- Exif#[](tag)

return the value (string) of the (({tag})). If the tag is not supported by current IFD, exception ((<Exif::Error::TagNotFound>)) raises. Unless you have set IFD by ((<Exif#ifd=>)), this method tries to find  the tag and its value over available IFDs.

--- Exif#each_entry(use_tag_id=false) {|tag, val| ...}

executes the block, with the block arguments being tag title ((|tag|)) over the LFDs and its value ((|val|)). If ((|use_tag_id|)) is ((|true|)), ((|tag|)) is set to tag ID (denoted by '0xdddd').

--- Exif#extract_thumbnail(dest)
--- Exif#thumbnail(dest)

write the thumbnail of the image to ((|dest|)), using (({dest#<<})). If the image does not have thumbnail, exception ((<Exif::Error::ThumbnailNotFound>)) raises.

== modules

== Exif::IFD

IFDs defined in libexif.

--- Exif::IFD::Zero
--- Exif::IFD::One
--- Exif::IFD::EXIF
--- Exif::IFD::GPS
--- Exif::IFD::Interoperability
--- Exif::IFD::Any
This constant should be set in ((<Exif#ifd=>)) if ((<Exif#[]>)) searches tag over above five IFDs. Exif object is set to this value by default.

== Exif::ByteOrder

constants that represents byte order of the EXIF image.

--- Exif::ByteOrder::INTEL
--- Exif::ByteOrder::MOTOROLA

== Exception classes

 RuntimeError --- Exif::Error -+- Exif::Error::NotExifFormat
                               |- Exif::Error::InvalidIFD
                               |- Exif::Error::ThumbnailNotFound
                               `- Exif::Error::TagNotFound
--- Exif::Error

Runtime errors occurred by using ruby-libexif belong to this class, or following derived classes.

--- Exif::Error::NotExifFormat
--- Exif::Error::InvalidIFD
--- Exif::Error::TagNotFound
--- Exif::Error::ThumbnailNotFound

=end
